﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Sprint {
    [HarmonyPatch(typeof(PlayerComponent))]
    [HarmonyPatch("Update")]
    internal class PlayerComponent_Update_Patch {
        [HarmonyPrefix]
        public static bool Prefix(PlayerComponent __instance)
        {
            bool toggleMode = Configuration.getInstance().getToggleMode();
            if (toggleMode)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    if (__instance.wgo.is_player)
                    {
                        if (MainGame.me.player.GetParam("isSprinting") != 1)
                        {
                            MainGame.me.player.SetParam("isSprinting", 1);
                        }
                        else
                        {
                            MainGame.me.player.SetParam("isSprinting", 0);
                        }
                    }
                }
            }
            return true;
        }
    }
}
